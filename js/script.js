(function(jQuery) {

    var pass1 = jQuery('#password'),
        pass2 = jQuery('#password2'),
        email = jQuery('#email'),
        form  = jQuery('.registre');
        
    jQuery('.form-box input').val('');



    form.on('submit',function(e){
        e.preventDefault(); 

        if(jQuery('.form-box .valid.success').length == jQuery('.form-box .valid').length){
            alert("Ebaaaaaaa deu tudo certo =D");
        }
        else{
            alert("Tem algo errado =( ");   
        }
    });

    email.on('blur',function(){
        
        if (!/^\S+@\S+\.\S+$/.test(email.val())){
            email.parent().addClass('error').removeClass('success');
        }
        else{
            email.parent().removeClass('error').addClass('success');
        }
        
    });    

    pass1.complexify({minimumChars:6, strengthScaleFactor:0.7}, function(valid, complexity){
		var progressBar = jQuery('.form-box #complexity-bar');

		progressBar.toggleClass('progress-bar-success', valid);
		progressBar.toggleClass('progress-bar-danger', !valid);
		progressBar.css({'width': complexity + '%'});


        if(valid){
            pass2.removeAttr('disabled');
            
            pass1.parent()
                    .removeClass('error')
                    .addClass('success');
        }
        else{
            pass2.attr('disabled','true');
            
            pass1.parent()
                    .removeClass('success')
                    .addClass('error');
        }
	});

    pass2.on('keydown input',function(){
    
        if(pass2.val() == pass1.val()){
            
            pass2.parent()
                    .removeClass('error')
                    .addClass('success');
        }
        else{
            pass2.parent()
                    .removeClass('success')
                    .addClass('error');
        } 
    });

})(jQuery);